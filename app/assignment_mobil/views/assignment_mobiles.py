from flask import request
from app.ext.security import Auth
from app.roles.models.Roles import Roles
from app.ext.security.auth_cached import Auth as auth
from app.ext.rest import Rest, HttpStatus
from app.ext.resource_handler import ResourceHandler
from app.commerce.models.Commerces import Commerces as Model_Commerces


class ViewAssignmentMobiles(ResourceHandler):
    decorators = [
        auth.require_auth_session,
    ]

    def get(self, id=0):
        return Rest.response(400, HttpStatus.RESOURCE_NOT_EXIST)

    @auth.has_role_of("SUPER_ADMIN")    
    def post(self):
        return Rest.response(400, HttpStatus.RESOURCE_NOT_EXIST)
        
    @auth.has_role_of("SUPER_ADMIN", "GENERAL_ADMINISTRATOR")
    @Auth.validate_request("mobiles_id")
    def put(self, id=0):
        if id == 0:
            return Rest.response(400, HttpStatus.DEFAULT_ERROR_MESSAGE, {'reason': 'use ID and retry again'})
        else:
            content = request.get_json()
            mobiles_id = content.get('mobiles_id', None)
            try:                
                isType = type(mobiles_id) is list
                if isType == True:
                    commerces_to_find = Model_Commerces.get_by_id(id)
                    if len(mobiles_id) > 0:
                        for list_ in mobiles_id:
                            commerces_to_find['mobiles'].append(list_)
                        result = Model_Commerces.update(id, commerces_to_find)
                    else:    
                        return Rest.response(400, HttpStatus.DEFAULT_ERROR_MESSAGE, {'reason': 'use mobiles and retry again'})
                    if result is None:
                        return Rest.response(200, HttpStatus.OK, {'message': 'mobiles assignment successfully!'})
                    else:
                        print("update_doc result error:", result)
                        return Rest.response(400, HttpStatus.UNEXPECTED_ERROR, {'reason': str(result)})
                else:   
                    return Rest.response(400, HttpStatus.UNEXPECTED_ERROR, {'reason': 'A list is expected'})     

            except Exception as e:
                print ("RoleView POST Exception:", e)
                return Rest.response(400, HttpStatus.UNEXPECTED_ERROR, {'reason': str(e)})

    @auth.has_role_of("SUPER_ADMIN", "GENERAL_ADMINISTRATOR")
    def delete(self, id=None):
        if id is None:
            return Rest.response(400, HttpStatus.DEFAULT_ERROR_MESSAGE, {'reason': 'use ID and retry again'})
        else:
            content = request.get_json()
            mobiles_id = content.get('mobiles_id', None)
            try:
                isType = type(mobiles_id) is list
                if isType == True:
                    commerces_to_find = Model_Commerces.get_by_id(id)
                    if len(mobiles_id) > 0:
                        if len(commerces_to_find['mobiles']) > 0:
                            for list_ in mobiles_id:
                                commerces_to_find['mobiles'].remove(list_)
                            result = Model_Commerces.update(id, commerces_to_find)
                        else:
                            return Rest.response(400, HttpStatus.DEFAULT_ERROR_MESSAGE, {'reason': 'empty list!'})        
                    else:    
                        return Rest.response(400, HttpStatus.DEFAULT_ERROR_MESSAGE, {'reason': 'use mobiles and retry again'})
                    if result is None:
                        return Rest.response(200, HttpStatus.OK, {'message': 'mobiles delete successfully!'})
                    else:
                        print("update_doc result error:", result)
                        return Rest.response(400, HttpStatus.UNEXPECTED_ERROR, {'reason': str(result)})
                else:   
                    return Rest.response(400, HttpStatus.UNEXPECTED_ERROR, {'reason': 'A list is expected'}) 

            except Exception as e:
                print ("RoleView POST Exception:", e)
                return Rest.response(400, HttpStatus.UNEXPECTED_ERROR, {'reason': str(e)})

