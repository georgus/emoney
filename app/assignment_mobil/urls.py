from app.assignment_mobil.views.assignment_mobiles import ViewAssignmentMobiles

from app.ext.register import url

urlpatterns = [
    url(ViewAssignmentMobiles, endpoint=['/assignmentMobiles', '/assignmentMobiles/<string:id>'], namespace="View_AssignmentMobiles"),
]
