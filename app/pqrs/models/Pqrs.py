# -*- coding: utf-8 -*-

from app.ext.firebase.firestore_model import FirestoreModel
from app.ext.utils import DateUtils


class Pqrs(FirestoreModel):
    __tablename__ = 'pqr'
    commerce_name = None
    commerce_id = None
    mac_mobil = None
    mobil_id = None
    issue = None
    date = None
    state = None
    created_at = DateUtils.get_timestamp()
    updated_at = DateUtils.get_timestamp()
