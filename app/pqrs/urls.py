from app.pqrs.views.pqrs import ViewPqrs

from app.ext.register import url

urlpatterns = [
    url(ViewPqrs, endpoint=['/pqrs', '/pqrs/<string:id>'], namespace="View_Pqrs"),
]
