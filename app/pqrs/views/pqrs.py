from flask import request
from app.ext.security import Auth
from app.ext.security.auth_cached import Auth as auth
from app.ext.rest import Rest, HttpStatus
from app.ext.resource_handler import ResourceHandler
from app.users.models.Users import Users as Model_users
from app.pqrs.models.Pqrs import Pqrs as Model_Pqrs
import hashlib
import datetime

class ViewPqrs(ResourceHandler):
    decorators = [
        auth.require_auth_session,
    ]

    @auth.has_role_of("SUPER_ADMIN", "GENERAL_ADMINISTRATOR")
    def get(self, id=0):
        if id == 0:
            info_session = auth.info_session()
            if(info_session['origin'] == 'WEB'):
                ref = Model_Pqrs.get_ref()
                query = ref.where(
                'commerce_id', '==', info_session['commerce_id']
                ).get()

                result = Model_Pqrs.to_json(query, True)
            elif(info_session['origin'] == 'GOD'):    
                result = Model_Pqrs.get_all()
            else:
                return Rest.response(400, HttpStatus.DEFAULT_ERROR_MESSAGE, {'reason': 'Model_Pqrs are empty'})

            array = []
            #print(result)
            
            if len(result) > 0:
                for list_result in result:
                    json = {
                        "key": list_result['key'],
                        "commerce_name": list_result['commerce_name'],
                        "mac_mobil": list_result['mac_mobil'],
                        "issue": list_result['issue'],
                        "date": list_result['date'],
                        "state": list_result['state'],
                    }
                    array.append(json)
                return Rest.response(200, HttpStatus.OK, array)
            else:
                return Rest.response(400, HttpStatus.DEFAULT_ERROR_MESSAGE, {'reason': 'Model_Pqrs are empty'})
        else:
            result = Model_users.get_by_id(id)
            if result is not None:
                    return Rest.response(200, HttpStatus.OK, result)
            else:
                return Rest.response(400, HttpStatus.RESOURCE_NOT_EXIST)
    
    @auth.has_role_of("SUPER_ADMIN", "GENERAL_ADMINISTRATOR", "USER_OPERATIONAL")
    @Auth.validate_request("issue")
    def post(self):
        content = request.get_json()
        issue = content.get('issue', None)
        state = "POR REVISION"
        date = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())

        try:
            info_session = auth.info_session()

            _new_pqr = Model_Pqrs()
            _new_pqr.commerce_name = info_session['commerce_name']
            _new_pqr.commerce_id = info_session['commerce_id']
            _new_pqr.mac_mobil = info_session['mac_mobil']
            _new_pqr.mobil_id = info_session['mobil_id']
            _new_pqr.issue = issue
            _new_pqr.state = str(state)
            _new_pqr.date = date
            Model_Pqrs.save(_new_pqr)
            return Rest.response(200, HttpStatus.OK, {'message': 'mobiles created successfully!'})
        except Exception as e:
            print ("usersView POST Exception:", e)
            return Rest.response(400, HttpStatus.UNEXPECTED_ERROR, {'reason': str(e)})
    @auth.has_role_of("SUPER_ADMIN", "GENERAL_ADMINISTRATOR")
    def put(self, id=0):
        if id == 0:
            return Rest.response(400, HttpStatus.DEFAULT_ERROR_MESSAGE, {'reason': 'use ID and retry again'})
        else:
            content = request.get_json()
            model = content.get('model', None)
            mac = content.get('mac', None)
            state = content.get('state', None)
            

            try:
                rol_to_find = Model_Pqrs.get_by_id(id)
                rol_to_find['model'] = model
                rol_to_find['mac'] =  mac
                rol_to_find['state'] = state
                result = Model_Pqrs.update(id, rol_to_find)

                if result is None:
                    return Rest.response(200, HttpStatus.OK, {'message': 'Mobiles updated successfully!'})
                else:
                    print("update_doc result error:", result)
                    return Rest.response(400, HttpStatus.UNEXPECTED_ERROR, {'reason': str(result)})

            except Exception as e:
                print ("RoleView POST Exception:", e)
                return Rest.response(400, HttpStatus.UNEXPECTED_ERROR, {'reason': str(e)})
    @auth.has_role_of("SUPER_ADMIN", "GENERAL_ADMINISTRATOR")
    def delete(self, id=None):
        if id is None:
            return Rest.response(400, HttpStatus.DEFAULT_ERROR_MESSAGE, {'reason': 'use ID and retry again'})
        else:
            result = Model_Pqrs.delete(id)
            if result is None:
                return Rest.response(200, HttpStatus.OK, {'message': 'Rol delete successfully!'})
            else:
                print("delete result error:", result)
                return Rest.response(400, HttpStatus.UNEXPECTED_ERROR, {'reason': str(result)})

