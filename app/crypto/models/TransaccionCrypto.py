# -*- coding: utf-8 -*-

from app.ext.firebase.firestore_model import FirestoreModel
from app.ext.utils import DateUtils


class TransaccionCrypto(FirestoreModel):
    __tablename__ = 'transaccion_crypto'
    ticket = None
    amount_cop = None
    amount_crypto = None
    comerce_id = None
    commerce_name = None
    datetime = None
    mac_mobil = None
    network = None
    created_at = DateUtils.get_timestamp()
    updated_at = DateUtils.get_timestamp()
